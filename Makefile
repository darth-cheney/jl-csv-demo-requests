all:
	python3 app.py

fmt:
	black *.py
	isort *.py

test:
	npm test
