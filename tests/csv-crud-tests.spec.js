import path from "path";
import * as api from "../index.js";
import chai from "chai";
const { assert, expect } = chai;

const EMPTY_VALUE = "";
const EXAMPLE_FILENAME = "example.csv";

describe("CSV CRUD Tests", () => {
  describe("#createCSV", () => {
    it("Can upload the example CSV", async () => {
      const filePath = path.resolve("./example.csv");
      const res = await api.uploadCSVFile(filePath);
      expect(res.status).to.equal(201);
    });
  });
  describe("#readCSVData", () => {
    const expectedData = {
      origin: [0, 1],
      corner: [5, 3],
      data: [
        [
          "100",
          "Rayna",
          "Romelda",
          "Rayna.Romelda@yopmail.com",
          "Rayna.Romelda@gmail.com",
          "firefighter",
        ],
        [
          "101",
          "Candy",
          "Mullane",
          "Candy.Mullane@yopmail.com",
          "Candy.Mullane@gmail.com",
          "developer",
        ],
        [
          "102",
          "Karina",
          "Euridice",
          "Karina.Euridice@yopmail.com",
          "Karina.Euridice@gmail.com",
          "firefighter",
        ],
      ],
    };
    let response;
    beforeEach(async () => {
      response = await api.readCSVData(
        EXAMPLE_FILENAME,
        expectedData.origin,
        expectedData.corner
      );
    });
    it("returns a 200 status", async () => {
      expect(response.status).to.equal(200);
    });
    it("has a Content-Type of application/json", async () => {
      expect(response.headers.get("Content-Type")).to.equal("application/json");
    });
    it("can read CSV data from origin to contained corner", async () => {
      const data = await response.json();
      expect(data).to.deep.equal(expectedData);
    });
  });
  describe("#deleteCSVData", () => {
    it("deletes some subframe of the data, using explicit origin and corner", async () => {
      const origin = [1, 1];
      const corner = [3, 4];
      const expectedData = {
        origin,
        corner,
      };

      const response = await api.deleteCSVData(
        EXAMPLE_FILENAME,
        origin,
        corner
      );
      expect(response.status).to.equal(200);
      const data = await response.json();
      expect(data).to.deep.equal(expectedData);
    });
    it("has correct data on subsequent read", async () => {
      const expectedOrigin = [0, 0];
      const expectedCorner = [4, 5];
      const expectedReadData = {
        origin: expectedOrigin,
        corner: expectedCorner,
        data: [
          ["id", "firstname", "lastname", "email", "email2"],
          [
            "100",
            EMPTY_VALUE,
            EMPTY_VALUE,
            EMPTY_VALUE,
            "Rayna.Romelda@gmail.com",
          ],
          [
            "101",
            EMPTY_VALUE,
            EMPTY_VALUE,
            EMPTY_VALUE,
            "Candy.Mullane@gmail.com",
          ],
          [
            "102",
            EMPTY_VALUE,
            EMPTY_VALUE,
            EMPTY_VALUE,
            "Karina.Euridice@gmail.com",
          ],
          [
            "103",
            EMPTY_VALUE,
            EMPTY_VALUE,
            EMPTY_VALUE,
            "Susan.Fosque@gmail.com",
          ],
          [
            "104",
            "Beatriz",
            "Craggie",
            "Beatriz.Craggie@yopmail.com",
            "Beatriz.Craggie@gmail.com",
          ],
        ],
      };

      const response = await api.readCSVData(
        EXAMPLE_FILENAME,
        expectedOrigin,
        expectedCorner
      );
      expect(response.status).to.equal(200);
      const data = await response.json();
      expect(data).to.deep.equal(expectedReadData);
    });
  });

  describe("#updateCSVData", () => {
    describe("updating CSV values sliently (without returning changed values)", () => {
      let response;
      it("responds with status 200", async () => {
        const origin = [1, 17];
        // The implied corner is [2, 18];
        const newRows = [
          ["Fred", "Flintstone"],
          ["Barney", "Rubble"],
        ];

        response = await api.updateCSVData(EXAMPLE_FILENAME, newRows, origin);
        expect(response.status).to.equal(200);
      });

      it("has the expected response data", async () => {
        // Because the 'silent' param is set to
        // true implicitly, we _do not_ return the
        // changed data rows with the response,
        // just the origin and corner
        const expectedData = {
          origin: [1, 17],
          corner: [2, 18],
        };
        const data = await response.json();

        expect(data).to.deep.equal(expectedData);
      });

      it("has the expected data present on subsequent read", async () => {
        const origin = [1, 17];
        const corner = [2, 18];
        const newRows = [
          ["Fred", "Flintstone"],
          ["Barney", "Rubble"],
        ];
        const expectedReadData = {
          origin: origin,
          corner: corner,
          data: newRows,
        };

        response = await api.readCSVData(EXAMPLE_FILENAME, origin, corner);
        const result = await response.json();

        expect(result).to.deep.equal(expectedReadData);
      });
    });
    describe("updating CSV values non-silently (returns changed values)", () => {
      const origin = [1, 17];
      const corner = [2, 18];
      const newRows = [
        ["Fred", "Flintstone"],
        ["Barney", "Rubble"],
      ];
      let response;

      it("returns status 200", async () => {
        response = await api.updateCSVData(
          EXAMPLE_FILENAME,
          newRows,
          origin,
          false
        );

        expect(response.status).to.equal(200);
      });

      it("responds with the expected data, including changed values", async () => {
        // Because we explicitly set the 'silent'
        // param to false, we _will_ return the
        // changed data rows in the response
        const expectedData = {
          origin,
          corner,
          data: newRows,
        };

        const result = await response.json();

        expect(result).to.deep.equal(expectedData);
      });

      it("responds with correct changed data on subsequent read", async () => {
        const expectedData = {
          origin,
          corner,
          data: newRows,
        };

        response = await api.readCSVData(EXAMPLE_FILENAME, origin, corner);
        const result = await response.json();

        expect(result).to.deep.equal(expectedData);
      });
    });
  });
});
