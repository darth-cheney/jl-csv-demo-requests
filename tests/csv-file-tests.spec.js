import path from "path";
import fs from "fs";
import * as files from "../files.js";
import * as api from "../index.js";
import chai from "chai";
const { assert, expect } = chai;

describe("Basic CSV File Operations", () => {
  describe("Upload a CSV", () => {
    it("Can upload the example CSV", async () => {
      const filePath = path.resolve("./example.csv");
      const res = await api.uploadCSVFile(filePath);
      expect(res.status).to.equal(201);
    });
  });

  describe("List files", () => {
    let response;
    beforeEach(async () => {
      response = await files.listFiles();
    });

    it("responds with 200", () => {
      expect(response.status).to.equal(200);
    });

    it("responds with an array of one (example) file", async () => {
      const data = await response.json();

      expect(data).to.deep.equal(["example.csv"]);
    });
  });

  describe("Download file", () => {
    let fileText;
    before(() => {
      fileText = fs.readFileSync(path.resolve("example.csv")).toString();
    });

    it("downloads the example file", async () => {
      const response = await files.downloadFile("example.csv");
      expect(response.status).to.equal(200);

      const result = await response.text();
      expect(result).to.equal(fileText);
    });
  });

  describe("Rename file", () => {
    let response;
    before(async () => {
      response = await files.renameFile("example.csv", "example2.csv");
    });

    it("accepts the rename request at 200", () => {
      expect(response.status).to.equal(200);
    });

    it("responds with the correct name in the file list", async () => {
      response = await files.listFiles();
      const data = await response.json();

      expect(data).to.deep.equal(["example2.csv"]);
    });
  });

  describe("Delete file", () => {
    let response;
    before(async () => {
      const filePath = path.resolve("./example.csv");
      await api.uploadCSVFile(filePath);
    });

    it("listFiles now has 2 files available", async () => {
      const res = await files.listFiles();
      const result = await res.json();

      result.sort();
      expect(result).to.deep.equal(["example.csv", "example2.csv"]);
    });

    it("returns 200 upon successful deletion request", async () => {
      response = await files.deleteFile("example2.csv");

      expect(response.status).to.equal(200);
    });

    it("after deletion, the file is not present in a listing", async () => {
      const res = await files.listFiles();
      const result = await res.json();

      expect(result).to.not.include("example2.csv");
    });
  });
});
