import path from "path";
import fetch from "node-fetch";
import fs from "fs";
import { URLSearchParams } from "url";

export const ROOT_URL = "http://localhost:1234";
const EMPTY_VALUE = "";

/**
 * CREATE (POST)
 * Uploads the initial CSV file.
 */
export const uploadCSVFile = async (filePath) => {
  const stats = fs.statSync(filePath);
  const fileSizeInBytes = stats.size;
  const readStream = fs.createReadStream(filePath);

  const fileName = path.basename(filePath);
  const url = `${ROOT_URL}/${fileName}`;

  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Length": fileSizeInBytes,
      user: "twn",
    },
    body: readStream,
  });
};

/**
 * GET DATA (GET)
 * Retrieve JSON data, including an ndarray of
 * rows, for the sliced csv data starting at the
 * origin coordinate and ending at the corner
 * coordinate.
 * Coordinates are size 2 arrays of x,y pairs.
 */
export const readCSVData = async (fileName, origin, corner) => {
  // We include the origin and corner in the querystring.
  // So the following:
  //     var origin = [0,0]
  //     var corner = [15, 234]
  //     URLSearchParams { 'origin' => '0,0', 'corner' => '15,234' }
  // yields a querystring of:
  //     'origin=0%2C0&corner=15%2C234'
  const searchParams = new URLSearchParams({
    origin,
    corner,
  });
  const url = `${ROOT_URL}/${fileName}?${searchParams}`;

  return fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      user: "twn",
    },
  });
};

/**
 * UPDATE (PUT)
 * Update values in the CSV with the provided data
 * rows, beginning at the specified origin
 * As a response, we expect the origin and corner
 * of the actually updated area.
 * If `silent` is false, we will also expect
 * data rows in the response.
 */
export const updateCSVData = async (
  fileName,
  rows,
  origin = [0, 0],
  silent = true
) => {
  const url = `${ROOT_URL}/${fileName}`;

  return fetch(url, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      user: "twn",
    },
    body: JSON.stringify({
      origin,
      rows,
      silent,
    }),
  });
};

/**
 * DELETE (DELETE)
 * Clear the CSV data starting at the given
 * origin and ending at the specified corner.
 * If corner is null, delete to the end of the
 * current CSV.
 * Origin and corner values are given in the
 * querystring.
 * For example, calling deleteCSVData() without
 * parameters will delete the whole file, since the
 * default origin is 0,0 and default corner is null.
 * If the X-DELETE-FILE header is present and has
 * a value of 'true', then the querystring is ignored
 * and the CSV file should be deleted entirely
 * rather than merely having its values cleared out.
 */
export const deleteCSVData = async (
  fileName,
  origin = [0, 0],
  corner = null,
  deleteEntirely = false
) => {
  const searchParams = new URLSearchParams({
    origin,
    corner,
  });
  const headers = {
    Accept: "application/json",
    user: "twn",
  };
  if (deleteEntirely) {
    headers["X-DELETE-FILE"] = "true";
  }
  const url = `${ROOT_URL}/${fileName}?${searchParams}`;

  return fetch(url, {
    method: "DELETE",
    headers: headers,
  });
};
