import fetch from "node-fetch";
import fs from "fs";
import { URLSearchParams } from "url";
import { ROOT_URL } from "./index.js";

export const listFiles = async () => {
  const url = `${ROOT_URL}/project/`;
  return fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      user: "twn",
    },
  });
};

export const downloadFile = async (fileName) => {
  const url = `${ROOT_URL}/project/${fileName}`;
  return fetch(url, {
    method: "GET",
    headers: {
      user: "twn",
    },
  });
};

export const renameFile = async (originalName, newName) => {
  const url = `${ROOT_URL}/project/${originalName}/update-name/${newName}`;

  return fetch(url, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "text",
      user: "twn",
    },
  });
};

export const deleteFile = async (fileName) => {
  const url = `${ROOT_URL}/project/${fileName}`;

  return fetch(url, {
    method: "DELETE",
    headers: {
      user: "twn",
    },
  });
};
