# jl-csv-demo-requests

## Installing ##
Run the following in order:
```shell
nodeenv --prebuilt --node=16.14.2 nodeenv
source nodeenv/bin/activate
npm install
```

## Running Tests ##
After installing, run `npm test`
