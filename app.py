import csv
import os
import sqlite3
import uuid
from io import StringIO

from flask import Flask, jsonify, make_response, request, send_file

app = Flask(__name__)

create_table_sql = """
CREATE TABLE IF NOT EXISTS files (
    user STRING,
    filename STRING,
    internal_filename STRING,
    num_cols INTEGER,
    num_rows INTEGER
)
"""

create_user_filename_idx = """
CREATE UNIQUE INDEX IF NOT EXISTS files_user_filename_idx ON files (user, filename)
"""

create_user_internal_filename_idx = """
CREATE UNIQUE INDEX IF NOT EXISTS files_user_internal_filename_idx ON files (user, internal_filename)
"""

create_internal_filename_idx = """
CREATE UNIQUE INDEX IF NOT EXISTS files_internal_filename_idx ON files (internal_filename)
"""


class State:
    def __init__(self, data_dir="data", db_path="data.db"):
        self._files = {}
        self._data_dir = data_dir
        os.makedirs(self._data_dir, exist_ok=True)
        self._db_path = db_path
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            cursor.execute(create_table_sql)
            cursor.execute(create_user_filename_idx)
            cursor.execute(create_user_internal_filename_idx)
            cursor.execute(create_internal_filename_idx)

    def _gc(self):
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = "SELECT internal_filename FROM files"
            tracked_files = set([filename for filename, in cursor.execute(query)])
        actual_files = set(os.listdir(self._data_dir))
        for filename in actual_files - tracked_files:
            filepath = f"{self._data_dir}/{filename}"
            os.remove(filepath)

    def create(self, user, filename, data):
        rows = list(csv.reader(StringIO(data)))
        header = rows[0]
        for row in rows:
            assert len(row) == len(header)
        num_cols = len(header)
        num_rows = len(rows)
        internal_filename = str(uuid.uuid4())
        internal_filepath = f"{self._data_dir}/{internal_filename}"
        with open(internal_filepath, "w") as outfile:
            writer = csv.writer(outfile)
            for row in rows:
                writer.writerow(row)
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = "DELETE FROM files WHERE user = ? AND filename = ?"
            payload = (user, filename)
            cursor.execute(query, payload)
            query = "INSERT INTO files (user, filename, internal_filename, num_cols, num_rows) VALUES (?, ?, ?, ?, ?)"
            payload = (user, filename, internal_filename, num_cols, num_rows)
            cursor.execute(query, payload)
            conn.commit()

    def read(self, user, filename, origin, corner):
        self._gc()
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = "SELECT internal_filename, num_cols, num_rows FROM files WHERE user = ? AND filename = ?"
            payload = (user, filename)
            internal_filename, num_cols, num_rows = cursor.execute(
                query, payload
            ).fetchone()
        assert corner[0] < num_cols
        assert corner[1] < num_rows
        with open(f"{self._data_dir}/{internal_filename}") as infile:
            rows = list(csv.reader(infile))
        return {
            "origin": origin,
            "corner": corner,
            "data": [
                row[origin[0] : corner[0] + 1]
                for row in rows[origin[1] : corner[1] + 1]
            ],
        }

    def delete(self, user, filename, origin, corner):
        self._gc()
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = "SELECT internal_filename, num_cols, num_rows FROM files WHERE user = ? AND filename = ?"
            payload = (user, filename)
            internal_filename, num_cols, num_rows = cursor.execute(
                query, payload
            ).fetchone()
        assert origin[0] <= corner[0] and corner[0] < num_cols
        assert origin[1] <= corner[1] and corner[1] < num_rows
        with open(f"{self._data_dir}/{internal_filename}") as infile:
            rows = list(csv.reader(infile))
        new_rows = []
        for row_num, row in enumerate(rows):
            new_row = []
            for col_num, val in enumerate(row):
                if (origin[0] <= col_num and col_num <= corner[0]) and (
                    origin[1] <= row_num and row_num <= corner[1]
                ):
                    new_row.append("")
                else:
                    new_row.append(val)
            new_rows.append(new_row)
        new_internal_filename = str(uuid.uuid4())
        new_internal_filepath = f"{self._data_dir}/{new_internal_filename}"
        with open(new_internal_filepath, "w") as outfile:
            writer = csv.writer(outfile)
            for row in new_rows:
                writer.writerow(row)
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = "DELETE FROM files WHERE user = ? AND filename = ?"
            payload = (user, filename)
            cursor.execute(query, payload)
            query = "INSERT INTO files (user, filename, internal_filename, num_cols, num_rows) VALUES (?, ?, ?, ?, ?)"
            payload = (user, filename, new_internal_filename, num_cols, num_rows)
            cursor.execute(query, payload)
            conn.commit()
        return {"origin": origin, "corner": corner}

    def update(self, user, filename, origin, offset_rows, silent):
        self._gc()
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = "SELECT internal_filename, num_cols, num_rows FROM files WHERE user = ? AND filename = ?"
            payload = (user, filename)
            internal_filename, num_cols, num_rows = cursor.execute(
                query, payload
            ).fetchone()
        corner = [origin[0] + len(offset_rows[0]) - 1, origin[1] + len(offset_rows) - 1]
        assert origin[0] <= corner[0] and corner[0] < num_cols
        assert origin[1] <= corner[1] and corner[1] < num_rows
        with open(f"{self._data_dir}/{internal_filename}") as infile:
            rows = list(csv.reader(infile))
        new_rows = []
        for row_num, row in enumerate(rows):
            new_row = []
            for col_num, val in enumerate(row):
                if (origin[0] <= col_num and col_num <= corner[0]) and (
                    origin[1] <= row_num and row_num <= corner[1]
                ):
                    new_row.append(
                        offset_rows[row_num - origin[1]][col_num - origin[0]]
                    )
                else:
                    new_row.append(val)
            new_rows.append(new_row)
        new_internal_filename = str(uuid.uuid4())
        new_internal_filepath = f"{self._data_dir}/{new_internal_filename}"
        with open(new_internal_filepath, "w") as outfile:
            writer = csv.writer(outfile)
            for row in new_rows:
                writer.writerow(row)
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = "DELETE FROM files WHERE user = ? AND filename = ?"
            payload = (user, filename)
            cursor.execute(query, payload)
            query = "INSERT INTO files (user, filename, internal_filename, num_cols, num_rows) VALUES (?, ?, ?, ?, ?)"
            payload = (user, filename, new_internal_filename, num_cols, num_rows)
            cursor.execute(query, payload)
            conn.commit()
        result = {"origin": origin, "corner": [2, 18]}
        if not silent:
            result["data"] = offset_rows
        return result

    def list_files(self, user):
        self._gc()
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = "SELECT filename FROM files WHERE user = ?"
            payload = (user,)
            filenames = list([filename for filename, in cursor.execute(query, payload)])
        return filenames

    def download_file(self, user, filename):
        self._gc()
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = (
                "SELECT internal_filename FROM files WHERE user = ? AND filename = ?"
            )
            payload = (user, filename)
            (internal_filename,) = cursor.execute(query, payload).fetchone()
        internal_filepath = f"{self._data_dir}/{internal_filename}"
        return send_file(
            internal_filepath,
            mimetype="text/csv",
            as_attachment=True,
            download_name=filename,
        )

    def rename_file(self, user, old_filename, new_filename):
        self._gc()
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = "UPDATE files SET filename = ? where user = ? AND filename = ?"
            payload = (new_filename, user, old_filename)
            cursor.execute(query, payload)
            conn.commit()
        return ""

    def delete_file(self, user, filename):
        self._gc()
        with sqlite3.connect(self._db_path) as conn:
            cursor = conn.cursor()
            query = "DELETE FROM files where user = ? AND filename = ?"
            payload = (user, filename)
            cursor.execute(query, payload)
            conn.commit()
        return ""


state = State()


@app.route("/project/", methods=["GET"])
def project_get():
    user = request.headers["user"]
    return make_response(jsonify(state.list_files(user)), 200)


@app.route("/project/<filename>", methods=["GET"])
def project_get_file(filename):
    user = request.headers["user"]
    return make_response(state.download_file(user, filename), 200)


@app.route("/project/<old_filename>/update-name/<new_filename>", methods=["PUT"])
def project_rename_file(old_filename, new_filename):
    user = request.headers["user"]
    return make_response(state.rename_file(user, old_filename, new_filename), 200)


@app.route("/project/<filename>", methods=["DELETE"])
def project_delete_file(filename):
    user = request.headers["user"]
    return make_response(state.delete_file(user, filename), 200)


@app.route("/<filename>", methods=["GET"])
def root_get(filename):
    user = request.headers["user"]
    origin = [int(val) for val in request.args["origin"].split(",")]
    corner = [int(val) for val in request.args["corner"].split(",")]
    return make_response(jsonify(state.read(user, filename, origin, corner)), 200)


@app.route("/<filename>", methods=["DELETE"])
def root_delete(filename):
    user = request.headers["user"]
    origin = [int(val) for val in request.args["origin"].split(",")]
    corner = [int(val) for val in request.args["corner"].split(",")]
    return make_response(jsonify(state.delete(user, filename, origin, corner)), 200)


@app.route("/<filename>", methods=["POST"])
def root_post(filename):
    user = request.headers["user"]
    data = request.data.decode(request.charset)
    state.create(user, filename, data)
    return make_response("", 201)


@app.route("/<filename>", methods=["PUT"])
def root_put(filename):
    user = request.headers["user"]
    origin = request.json["origin"]
    rows = request.json["rows"]
    silent = request.json["silent"]
    return make_response(
        jsonify(state.update(user, filename, origin, rows, silent)), 200
    )


if __name__ == "__main__":
    app.run(port=1234)
